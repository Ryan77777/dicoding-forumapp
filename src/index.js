import React, { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import App from "./App";
import store from "./redux";

import "./styles/style.scss";
import { ThemeProvider } from "./contexts/ThemeContext";

const root = createRoot(document.getElementById("root"));

root.render(
    <Provider store={store}>
        <BrowserRouter>
            <StrictMode>
                <ThemeProvider>
                    <App />
                </ThemeProvider>
            </StrictMode>
        </BrowserRouter>
    </Provider>
);
