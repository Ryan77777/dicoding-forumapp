import { hideLoading, showLoading } from "react-redux-loading-bar";
import API from "../../utils/api";
import { receiveThreadsActionCreator } from "../threads/action";
import { receiveUsersActionCreator } from "../users/action";

// ==============
// Thunk Function
// ==============

function asyncPopulateUsersAndThreads() {
    return async (dispatch) => {
        dispatch(showLoading());
        try {
            const users = await API.getAllUsers();
            const threads = await API.getAllThreads();

            dispatch(receiveUsersActionCreator(users));
            dispatch(receiveThreadsActionCreator(threads));
        } catch (error) {
            alert(error.message);
        }
        dispatch(hideLoading());
    };
}

export { asyncPopulateUsersAndThreads };
