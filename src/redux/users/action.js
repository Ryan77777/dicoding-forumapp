import { hideLoading, showLoading } from "react-redux-loading-bar";
import API from "../../utils/api";

// ==============
// Action Type
// ==============

const ActionType = {
    RECEIVE_USERS: "RECEIVE_USERS",
};

// ==============
// Action Creator
// ==============

function receiveUsersActionCreator(users) {
    return {
        type: ActionType.RECEIVE_USERS,
        payload: {
            users,
        },
    };
}

// ==============
// Thunk Function
// ==============

function asyncRegisterUser({ name, email, password }) {
    return async (dispatch) => {
        dispatch(showLoading());
        try {
            await API.register({ name, email, password });
        } catch (error) {
            alert(error.message);
        }
        dispatch(hideLoading());
    };
}

export { ActionType, receiveUsersActionCreator, asyncRegisterUser };
