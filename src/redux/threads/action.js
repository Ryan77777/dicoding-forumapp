import { hideLoading, showLoading } from "react-redux-loading-bar";
import API from "../../utils/api";

// ==============
// Action Type
// ==============

const ActionType = {
    RECEIVE_THREADS: "RECEIVE_THREADS",
    ADD_THREAD: "ADD_THREAD",
};

// ==============
// Action Creator
// ==============

function receiveThreadsActionCreator(threads) {
    return {
        type: ActionType.RECEIVE_THREADS,
        payload: {
            threads,
        },
    };
}

function addThreadActionCreator(thread) {
    return {
        type: ActionType.ADD_THREAD,
        payload: {
            thread,
        },
    };
}

// ==============
// Thunk Function
// ==============

function asyncAddThread({ title, body }) {
    return async (dispatch) => {
        dispatch(showLoading());
        try {
            console.log(title, body);
            const thread = await API.createThread({ title, body });
            dispatch(addThreadActionCreator(thread));
        } catch (error) {
            alert(error.message);
        }
        dispatch(hideLoading());
    };
}

export {
    ActionType,
    receiveThreadsActionCreator,
    addThreadActionCreator,
    asyncAddThread,
};
