import { hideLoading, showLoading } from "react-redux-loading-bar";
import API from "../../utils/api";

// ==============
// Action Type
// ==============

const ActionType = {
    RECEIVE_THREAD_DETAIL: "RECEIVE_THREAD_DETAIL",
    CLEAR_THREAD_DETAIL: "CLEAR_THREAD_DETAIL",
    ADD_COMMENT_THREAD: "ADD_COMMENT_THREAD",
};

// ==============
// Action Creator
// ==============

function receiveThreadDetailActionCreator(threadDetail) {
    return {
        type: ActionType.RECEIVE_THREAD_DETAIL,
        payload: {
            threadDetail,
        },
    };
}

function clearThreadDetailActionCreator() {
    return {
        type: ActionType.CLEAR_THREAD_DETAIL,
    };
}

function addCommentThreadActionCreator(comment) {
    return {
        type: ActionType.ADD_COMMENT_THREAD,
        payload: {
            comment,
        },
    };
}

// ==============
// Thunk Function
// ==============

function asyncReceiveThreadDetail(threadId) {
    return async (dispatch) => {
        dispatch(showLoading());
        dispatch(clearThreadDetailActionCreator());

        try {
            const detailThread = await API.getThreadDetail(threadId);
            dispatch(receiveThreadDetailActionCreator(detailThread));
        } catch (error) {
            alert(error.message);
        }
        dispatch(hideLoading());
    };
}

function asyncAddCommentThread({ content, threadId }) {
    return async (dispatch) => {
        dispatch(showLoading());
        try {
            const comment = await API.createComment({
                content,
                threadId,
            });
            dispatch(addCommentThreadActionCreator(comment));
        } catch (error) {
            alert(error.message);
        }
        dispatch(hideLoading());
    };
}

export {
    ActionType,
    receiveThreadDetailActionCreator,
    clearThreadDetailActionCreator,
    asyncReceiveThreadDetail,
    asyncAddCommentThread,
};
