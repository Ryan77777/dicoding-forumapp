import { hideLoading, showLoading } from "react-redux-loading-bar";
import API from "../../utils/api";
import { setAuthUserActionCreator } from "../authUser/action";

// ==============
// Action Type
// ==============

const ActionType = {
    SET_IS_PRELOAD: "SET_IS_PRELOAD",
};

// ==============
// Action Creator
// ==============

function setIsPreloadActionCreator(isPreLoad) {
    return {
        type: ActionType.SET_IS_PRELOAD,
        payload: {
            isPreLoad,
        },
    };
}

// ==============
// Thunk Function
// ==============

function asyncPreloadProcess() {
    return async (dispatch) => {
        dispatch(showLoading());
        try {
            // preload process
            const authUser = await API.getOwnProfile();
            dispatch(setAuthUserActionCreator(authUser));
        } catch (error) {
            // fallback process
            dispatch(setAuthUserActionCreator(null));
        } finally {
            // end preload process
            dispatch(setIsPreloadActionCreator(false));
        }
        dispatch(hideLoading());
    };
}

export { ActionType, setIsPreloadActionCreator, asyncPreloadProcess };
