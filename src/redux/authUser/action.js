import { hideLoading, showLoading } from "react-redux-loading-bar";
import API from "../../utils/api";

// ==============
// Action Type
// ==============

const ActionType = {
    SET_AUTH_USER: "SET_AUTH_USER",
    UNSET_AUTH_USER: "UNSET_AUTH_USER",
};

// ==============
// Action Creator
// ==============

function setAuthUserActionCreator(authUser) {
    return {
        type: ActionType.SET_AUTH_USER,
        payload: {
            authUser,
        },
    };
}

function unsetAuthUserActionCreator() {
    return {
        type: ActionType.UNSET_AUTH_USER,
        payload: {
            authUser: null,
        },
    };
}

// ==============
// Thunk Function
// ==============

function asyncSetAuthUser({ email, password }) {
    return async (dispatch) => {
        dispatch(showLoading());
        try {
            const token = await API.login({ email, password });
            API.putAccessToken(token);
            const authUser = await API.getOwnProfile();

            dispatch(setAuthUserActionCreator(authUser));
        } catch (error) {
            alert(error.message);
        }
        dispatch(hideLoading());
    };
}

function asyncUnsetAuthUser() {
    return (dispatch) => {
        dispatch(showLoading());
        dispatch(unsetAuthUserActionCreator());
        API.putAccessToken("");
        dispatch(hideLoading());
    };
}

export {
    ActionType,
    setAuthUserActionCreator,
    unsetAuthUserActionCreator,
    asyncSetAuthUser,
    asyncUnsetAuthUser,
};
