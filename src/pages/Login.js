import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import LoginInput from "../components/LoginInput";
import { asyncSetAuthUser } from "../redux/authUser/action";
import "../styles/login.scss";

const Login = () => {
    const dispatch = useDispatch();
    const onLogin = ({ email, password }) => {
        dispatch(asyncSetAuthUser({ email, password }));
    };

    return (
        <div className="login">
            <div className="card">
                <div className="left">
                    <h1>Hello World.</h1>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Libero cum, alias totam numquam ipsa exercitationem
                        dignissimos, error nam, consequatur.
                    </p>
                    <span>Don&apos;t you have an account?</span>
                    <Link to="/register">
                        <button>Register</button>
                    </Link>
                </div>
                <div className="right">
                    <h1>Login</h1>
                    <LoginInput login={onLogin} />
                </div>
            </div>
        </div>
    );
};

export default Login;
