import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PostInput from "../components/PostInput";
import PostsList from "../components/PostsList";
import { asyncPopulateUsersAndThreads } from "../redux/shared/action";
import { asyncAddThread } from "../redux/threads/action";
import "../styles/home.scss";


const Home = () => {
    const { threads = [], users = [] } = useSelector((states) => states);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(asyncPopulateUsersAndThreads());
    }, [dispatch]);

    const onAddThread = ({title, body}) => {
        dispatch(asyncAddThread({ title, body }));
    };

    const threadList = threads.map((thread) => ({
        ...thread,
        user: users.find((user) => user.id === thread.ownerId),
    }));

    return (
        <div className="home">
            <PostInput createThread={onAddThread} />
            <PostsList threads={threadList} />
        </div>
    );
};

export default Home;
