import React from "react";
import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import RegisterInput from "../components/RegisterInput";
import { asyncRegisterUser } from "../redux/users/action";
import "../styles/register.scss";


const Register = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const onRegister = ({ name, email, password }) => {
        dispatch(asyncRegisterUser({ name, email, password }));

        navigate("/");
    };
    return (
        <div className="register">
            <div className="card">
                <div className="left">
                    <h1>Forum App.</h1>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Libero cum, alias totam numquam ipsa exercitationem
                        dignissimos, error nam, consequatur.
                    </p>
                    <span>Do you have an account?</span>
                    <Link to="/login">
                        <button>Login</button>
                    </Link>
                </div>
                <div className="right">
                    <h1>Register</h1>
                    <RegisterInput register={onRegister} />
                </div>
            </div>
        </div>
    );
};

export default Register;
