import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import PostDetail from "../components/PostDetail";
import {
    asyncAddCommentThread,
    asyncReceiveThreadDetail,
} from "../redux/threadDetail/action";

const Detail = () => {
    const { id } = useParams();
    const { threadDetail = null } = useSelector((states) => states);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(asyncReceiveThreadDetail(id));
    }, [id, dispatch]);

    const onCommentThread = (content) => {
        dispatch(asyncAddCommentThread({ content: content.content, threadId: threadDetail?.id }));
    };

    if (!threadDetail) {
        return null;
    }

    return (
        <div className="home">
            <PostDetail {...threadDetail} sendComment={onCommentThread} />
        </div>
    );
};

export default Detail;
