import PropTypes from "prop-types";
import React from "react";
import "../styles/posts.scss";
import PostItem, { threadtemShape } from "./PostItem";

const PostsList = ({ threads }) => {
    return (
        <div className="posts">
            {threads.map((thread, index) => (
                <PostItem key={index} {...thread} />
            ))}
        </div>
    );
};

PostsList.propTypes = {
    threads: PropTypes.arrayOf(PropTypes.shape(threadtemShape)).isRequired,
};

export default PostsList;
