import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import ThumbDownOffAltIcon from "@mui/icons-material/ThumbDownOffAlt";
import ThumbUpOffAltIcon from "@mui/icons-material/ThumbUpOffAlt";
import parser from "html-react-parser";
import PropTypes from "prop-types";
import React from "react";
import "../styles/post.scss";
import { postedAt } from "../utils";
import Comments from "./Comments";

const PostDetail = ({
    id,
    body,
    category,
    createdAt,
    downVotesBy,
    title,
    upVotesBy,
    owner,
    comments,
    sendComment
}) => {
    return (
        <div className="post">
            <div className="container">
                <div className="user">
                    <div className="userInfo">
                        <img src={owner?.avatar} alt="User profile" />
                        <div className="details">
                            <span className="name">{owner?.name}</span>
                            <span className="date">{postedAt(createdAt)}</span>
                        </div>
                    </div>
                    <MoreHorizIcon />
                </div>
                <div className="content">
                    <b>{parser(title)}</b>

                    <br />
                    <p>
                        {parser(
                            body?.length > 500
                                ? body.slice(0, 500) + "..."
                                : body
                        )}
                    </p>
                </div>
                <div className="hashtag">
                    <p>#{category}</p>
                </div>
                <div className="info">
                    <div className="item">
                        <ThumbUpOffAltIcon />
                        {upVotesBy.length} Likes
                    </div>
                    <div className="item">
                        <ThumbDownOffAltIcon />
                        {downVotesBy.length} UnLikes
                    </div>
                </div>
                <Comments comments={comments} sendComment={sendComment} />
            </div>
        </div>
    );
};

const userShape = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
};

const commentsShape = {
    id: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    downVotesBy: PropTypes.arrayOf(PropTypes.string).isRequired,
    upVotesBy: PropTypes.arrayOf(PropTypes.string).isRequired,
    owner: PropTypes.shape(userShape).isRequired,
};

PostDetail.propTypes = {
    id: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    downVotesBy: PropTypes.arrayOf(PropTypes.string).isRequired,
    title: PropTypes.string.isRequired,
    upVotesBy: PropTypes.arrayOf(PropTypes.string).isRequired,
    owner: PropTypes.shape(userShape).isRequired,
    comments: PropTypes.arrayOf(PropTypes.shape(commentsShape)).isRequired,
    sendComment: PropTypes.func.isRequired,
};

export default PostDetail;
