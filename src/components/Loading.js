import React from "react";
import LoadingBar from "react-redux-loading-bar";
import "../styles/loading.scss";

function Loading() {
    return (
        <div className="loading">
            <LoadingBar />
        </div>
    );
}

export default Loading;
