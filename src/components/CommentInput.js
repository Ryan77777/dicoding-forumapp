import React from "react";
import { useSelector } from "react-redux";
import useInput from "../hooks/useInput";
import PropTypes from "prop-types";

function CommentInput({ sendComment }) {
    const { authUser } = useSelector((states) => states);

    const [content, onContentChange, setContent] = useInput("");

    return (
        <div className="write">
            <img src={authUser.avatar} alt="User profile" />
            <input
                type="text"
                value={content}
                onChange={onContentChange}
                placeholder="write a comment"
            />
            <button
                type="button"
                onClick={() => {
                    setContent("");
                    sendComment({ content });
                }}
            >
                Send
            </button>
        </div>
    );
}

CommentInput.protoTypes = {
    sendComment: PropTypes.func.isRequired,
};

export default CommentInput;
