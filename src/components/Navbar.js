import DarkModeOutlinedIcon from "@mui/icons-material/DarkModeOutlined";
import SiginOutIcon from "@mui/icons-material/Logout";
import WbSunnyOutlinedIcon from "@mui/icons-material/WbSunnyOutlined";
import PropTypes from "prop-types";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { ThemeContext } from "../contexts/ThemeContext";
import "../styles/navbar.scss";

const Navbar = ({ logout }) => {
    const { toggle, theme } = useContext(ThemeContext);

    return (
        <div className="navbar">
            <div className="left">
                <Link to="/" style={{ textDecoration: "none" }}>
                    <span>Forum App</span>
                </Link>
            </div>
            <div className="right">
                {theme ? (
                    <WbSunnyOutlinedIcon onClick={toggle} style={{ cursor: "pointer" }} />
                ) : (
                    <DarkModeOutlinedIcon onClick={toggle} style={{ cursor: "pointer" }} />
                )}
                <SiginOutIcon onClick={logout} style={{ cursor: "pointer" }} />
            </div>
        </div>
    );
};

Navbar.propTypes = {
    logout: PropTypes.func.isRequired,
};

export default Navbar;
