import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import TextsmsOutlinedIcon from "@mui/icons-material/TextsmsOutlined";
import ThumbDownOffAltIcon from "@mui/icons-material/ThumbDownOffAlt";
import ThumbUpOffAltIcon from "@mui/icons-material/ThumbUpOffAlt";
import parser from "html-react-parser";
import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import "../styles/post.scss";
import { postedAt } from "../utils";


const PostItem = ({
    id,
    body,
    category,
    createdAt,
    downVotesBy,
    title,
    totalComments,
    upVotesBy,
    user,
}) => {
    return (
        <div className="post">
            <div className="container">
                <div className="user">
                    <div className="userInfo">
                        <img src={user?.avatar} alt="User profile" />
                        <div className="details">
                            <span className="name">{user?.name}</span>
                            <span className="date">{postedAt(createdAt)}</span>
                        </div>
                    </div>
                    <MoreHorizIcon />
                </div>
                <div className="content">
                    <Link to={`thread/${id}`} className="title">
                        <b>{parser(title)}</b>
                    </Link>

                    <br />
                    <br />
                    <span>
                        {parser(
                            body.length > 500
                                ? body.slice(0, 500) + "..."
                                : body
                        )}
                    </span>
                </div>
                <div className="hashtag">
                    <p>#{category}</p>
                </div>
                <div className="info">
                    <div className="item">
                        <ThumbUpOffAltIcon />
                        {upVotesBy.length} Likes
                    </div>
                    <div className="item">
                        <ThumbDownOffAltIcon />
                        {downVotesBy.length} UnLikes
                    </div>
                    <div className="item">
                        <TextsmsOutlinedIcon />
                        {totalComments} Comments
                    </div>
                </div>
            </div>
        </div>
    );
};

const userShape = {
    id: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
};

const threadtemShape = {
    id: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    downVotesBy: PropTypes.arrayOf(PropTypes.string).isRequired,
    title: PropTypes.string.isRequired,
    totalComments: PropTypes.number.isRequired,
    upVotesBy: PropTypes.arrayOf(PropTypes.string).isRequired,
    user: PropTypes.shape(userShape).isRequired,
};

export { threadtemShape };

export default PostItem;
