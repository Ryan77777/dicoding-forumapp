import React from "react";
import { useSelector } from "react-redux";
import useInput from "../hooks/useInput";
import "../styles/input.scss";

const PostInput = ({ createThread }) => {
    const { authUser } = useSelector((states) => states);

    const [title, onTitleChange, setTitle] = useInput("");
    const [body, onBodyChange, setBody] = useInput("");
    return (
        <div className="share">
            <div className="container">
                <div className="top">
                    <img src={authUser.avatar} alt="User Profile" />
                    <div style={{width: "100%"}}>
                        <input
                            type="text"
                            value={title}
                            onChange={onTitleChange}
                            placeholder="Title"
                        />
                        <input
                            type="text"
                            value={body}
                            onChange={onBodyChange}
                            placeholder={`What's on your mind ${authUser.name}?`}
                        />
                    </div>
                </div>
                <hr />
                <div className="bottom">
                    <div className="left"></div>
                    <div className="right">
                        <button
                            type="button"
                            onClick={() => {
                                setTitle("");
                                setBody("");
                                createThread({ title, body });
                            }}
                        >
                            Share
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PostInput;
