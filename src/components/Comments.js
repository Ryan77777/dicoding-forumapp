import parser from "html-react-parser";
import PropTypes from "prop-types";
import React from "react";
import "../styles/comments.scss";
import { postedAt } from "../utils";
import CommentInput from "./CommentInput";


const Comments = ({ comments, sendComment }) => {
    return (
        <div className="comments">
            <CommentInput sendComment={sendComment} />

            {comments.length > 0 ? (
                comments.map((comment, index) => (
                    <div key={index} className="comment">
                        <img src={comment?.owner?.avatar} alt="" />
                        <div className="info">
                            <span>{comment?.owner?.name}</span>
                            <p>{parser(comment?.content)}</p>
                        </div>
                        <span className="date">
                            {postedAt(comment?.createdAt)}
                        </span>
                    </div>
                ))
            ) : (
                <p>Tidak ada komentar</p>
            )}
        </div>
    );
};

const userShape = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
};

const commentsShape = {
    id: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    downVotesBy: PropTypes.arrayOf(PropTypes.string).isRequired,
    upVotesBy: PropTypes.arrayOf(PropTypes.string).isRequired,
    owner: PropTypes.shape(userShape).isRequired,
};

Comments.propTypes = {
    comments: PropTypes.arrayOf(PropTypes.shape(commentsShape)).isRequired,
    sendComment: PropTypes.func.isRequired,
};

export default Comments;
