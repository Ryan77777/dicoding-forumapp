import React, { useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Route, Routes } from "react-router-dom";
import Loading from "./components/Loading";
import Navbar from "./components/Navbar";
import { ThemeContext } from "./contexts/ThemeContext";
import Detail from "./pages/Detail";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import { asyncUnsetAuthUser } from "./redux/authUser/action";
import { asyncPreloadProcess } from "./redux/isPreload/action";

function App() {
    const { theme } = useContext(ThemeContext);

    const { authUser = null, isPreload = false } = useSelector(
        (states) => states
    );

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(asyncPreloadProcess());
    }, [dispatch]);

    const onSignOut = () => {
        dispatch(asyncUnsetAuthUser());
    };

    if (isPreload) {
        return null;
    }

    if (authUser === null) {
        return (
            <>
                <main>
                <Loading />
                    <Routes>
                        <Route path="/*" element={<Login />} />
                        <Route path="/register" element={<Register />} />
                    </Routes>
                </main>
            </>
        );
    }

    return (
        <>
            <div className={`theme-${theme ? "dark" : "light"}`}>
                <Loading />
                <Navbar logout={onSignOut} />
                <div style={{ display: "flex" }}>
                    <div style={{ flex: 6 }}>
                        <Routes>
                            <Route path="/" element={<Home />} />
                            <Route path="/thread/:id" element={<Detail />} />
                        </Routes>
                    </div>
                </div>
            </div>
        </>
    );
}

export default App;
