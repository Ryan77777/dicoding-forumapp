import React, { createContext, useEffect, useState } from "react";

export const ThemeContext = createContext();

export const ThemeProvider = ({ children }) => {
    const [theme, setTheme] = useState(
        JSON.parse(localStorage.getItem("dark")) || false
        );
        
        const toggle = () => {
            setTheme(!theme);
        };
        
        useEffect(() => {
            localStorage.setItem("dark", theme);
        }, [theme]);
        
        return (
            <ThemeContext.Provider value={{ theme, toggle }}>
      {children}
    </ThemeContext.Provider>
  );
};

